package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.bl.entities.curso.ICurso;
import cr.ac.ucenfotec.bl.entities.curso.MySQLCursoImpl;

import java.util.ArrayList;

public class CursoGestor {

    private ICurso datos;

    public CursoGestor(){
        datos = new MySQLCursoImpl();
    }

    public String registrarCurso(String codigo, String nombre, int creditos) throws Exception{
        Curso tmpCurso = new Curso(codigo,nombre,creditos);

        return datos.registrarCurso(tmpCurso);
    }

    public ArrayList<Curso> getCursos() throws Exception{
        return datos.listarCursos();
    }
}
