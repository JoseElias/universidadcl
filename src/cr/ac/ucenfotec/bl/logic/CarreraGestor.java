package cr.ac.ucenfotec.bl.logic;

import cr.ac.ucenfotec.bl.entities.carrera.Carrera;
import cr.ac.ucenfotec.bl.entities.carrera.ICarrera;
import cr.ac.ucenfotec.bl.entities.carrera.MySQLCarreraImpl;
import cr.ac.ucenfotec.bl.entities.curso.Curso;
import cr.ac.ucenfotec.bl.entities.curso.ICurso;
import cr.ac.ucenfotec.bl.entities.curso.MySQLCursoImpl;

import java.util.ArrayList;

public class CarreraGestor {

    private ICarrera datos;
    private ICurso datosCurso;

    public CarreraGestor(){
        datos = new MySQLCarreraImpl();
        datosCurso = new MySQLCursoImpl();
    }

    public String registrarCarrera(String codigo,String nombre, boolean acreditada) throws Exception{
        Carrera tmpCarrera = new Carrera(codigo,nombre,acreditada);

        return datos.registrarCarrera(tmpCarrera);
    }

    public ArrayList<Carrera> getCarreras() throws Exception{
        return datos.listarCarreras();
    }

    public String asociarCurso(String codigoCurso,String codigoCarrera) throws Exception{
        Curso tmpCurso = datosCurso.buscarCurso(codigoCurso);

        if(tmpCurso != null){
            Carrera tmpCarrera = datos.buscarCarrera(codigoCarrera);
            if(tmpCarrera != null) {
                tmpCarrera.agregarCurso(tmpCurso);
            }else{
                return "El código de la carrera no existe";
            }
        }else{
            return "El código del curso no existe";
        }
        return "Curso agregado con éxito";
    }
}
