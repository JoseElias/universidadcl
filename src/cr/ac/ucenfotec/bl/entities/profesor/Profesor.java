package cr.ac.ucenfotec.bl.entities.profesor;
//la herencia es una relación de tipo es un.

import cr.ac.ucenfotec.bl.entities.Persona;

public class Profesor extends Persona {
    private String profesion;

    public Profesor() {
        super();
    }

    public Profesor(String nombre, String identificacion, String correo, String profesion) {
        super(nombre,identificacion,correo);
        this.profesion = profesion;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    @Override
    public String toString() {
        return "Profesor{" +
                "nombre='" + nombre + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", correo='" + correo + '\'' +
                ", profesion='" + profesion + '\'' +
                '}';
    }



}
