package cr.ac.ucenfotec.bl.entities.curso;

import java.util.ArrayList;

public interface ICurso {

    String registrarCurso(Curso curso) throws Exception;
    ArrayList<Curso> listarCursos() throws Exception;
    Curso buscarCurso(String codigoCurso) throws Exception;
}
