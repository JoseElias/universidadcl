package cr.ac.ucenfotec.bl.entities.estudiante;

import cr.ac.ucenfotec.bl.entities.Persona;
import cr.ac.ucenfotec.bl.entities.carrera.Carrera;

public class Estudiante extends Persona {
    private Carrera carreraCursada;

    public Estudiante() {
        super();//llamando al constructor del padre.;
    }

    public Estudiante(String nombre, String identificacion, String correo, Carrera carreraCursada) {
        super(nombre,identificacion,correo);//llamando al constructor del padre.
        this.carreraCursada = carreraCursada;
    }

    public Estudiante(String nombre, String identificacion, String correo) {
       super(nombre,identificacion,correo);
    }

    public Carrera getCarreraCursada() {
        return carreraCursada;
    }

    public void setCarreraCursada(Carrera carreraCursada) {
        this.carreraCursada = carreraCursada;
    }

    @Override
    public String toString() {
        return "Estudiante{" +
               super.toString() + '\'' +
                ", carreraCursada=" + carreraCursada +
                '}';
    }
}
