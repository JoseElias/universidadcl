package cr.ac.ucenfotec.bl.entities.carrera;

import java.util.ArrayList;

public interface ICarrera {

    String registrarCarrera(Carrera carrera) throws Exception;
    ArrayList<Carrera> listarCarreras() throws Exception;
    Carrera buscarCarrera(String codigoCarrera) throws Exception;

}
