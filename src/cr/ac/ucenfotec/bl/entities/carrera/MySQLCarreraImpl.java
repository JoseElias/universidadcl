package cr.ac.ucenfotec.bl.entities.carrera;

import cr.ac.ucenfotec.dl.bd.Conector;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MySQLCarreraImpl implements ICarrera {

    private String sqlQuery = "";

    public String registrarCarrera(Carrera carrera) throws Exception {
        sqlQuery = "INSERT INTO CARRERA....";
        Conector.getConnector().ejecutarSQL(sqlQuery);

        return "La Carrera de " + carrera.getNombre() + ", fue registrada correctamente!";
    }


    public ArrayList<Carrera> listarCarreras() throws Exception {
        ArrayList<Carrera> listaCarreras = new ArrayList<>();
        sqlQuery = "SELECT * FROM CARRERAS";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        while (rs.next()) {
            Carrera carrera = new Carrera(rs.getString("CODIGOCARRERA"), rs.getString("NOMBRE"), true);
            listaCarreras.add(carrera);
        }

        return listaCarreras;
    }


    public Carrera buscarCarrera(String codigoCarrera) throws Exception {
        Carrera carrera = null;
        sqlQuery="SELECT * FROM CARRERA WHERE CODIGOCARRERA='"+codigoCarrera+"'";
        ResultSet rs = Conector.getConnector().ejecutarQuery(sqlQuery);

        if(rs.next()){
            carrera = new Carrera(rs.getString("CODIGOCARRERA"),rs.getString("NOMBRE"),true);
        }

        return carrera;
    }
}
