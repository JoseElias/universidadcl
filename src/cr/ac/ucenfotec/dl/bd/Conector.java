package cr.ac.ucenfotec.dl.bd;

public class Conector {

    private static AccesoBD coneccionBD = null;

    public static AccesoBD getConnector() throws Exception{
        String URL = "jdbc:mysql://localhost/universidadbd?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        String user = "root";
        String password ="";

        if(coneccionBD == null){
            coneccionBD = new AccesoBD(URL,user,password);
        }
        return coneccionBD;
    }

}
