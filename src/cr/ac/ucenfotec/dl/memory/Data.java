package cr.ac.ucenfotec.dl.memory;

import cr.ac.ucenfotec.bl.entities.carrera.Carrera;
import cr.ac.ucenfotec.bl.entities.curso.Curso;
import java.util.ArrayList;

public class Data {

    private ArrayList<Carrera> carreras;
    private ArrayList<Curso> cursos;

    public Data() {
        carreras = new ArrayList<>();
        cursos = new ArrayList<>();
    }

    public String registrarCarrera(Carrera carrera){
        carreras.add(carrera);
        return "La Carrera " + carrera.getNombre() + ", fue registrada exitosamente!";
    }

    public ArrayList<Carrera> listarCarreras(){
        return (ArrayList<Carrera>) carreras.clone();
    }

    public Carrera buscarCarrera(String codigoCarrera){
        for(Carrera carrera: carreras){
            if(carrera.getCodigo().equals(codigoCarrera))
                return carrera;
        }
        return null;
    }

    public String registrarCurso(Curso curso){
        cursos.add(curso);
        return "El curso " + curso.getNombre() + ", fue registrado exitosamente!";
    }

    public ArrayList<Curso> listarCursos(){
        return (ArrayList<Curso>) cursos.clone();
    }

    public Curso buscarCurso(String codigoCurso){
        for(Curso curso: cursos){
          if(curso.getCodigo().equals(codigoCurso))
              return curso;
        }
        return null;
    }
}
